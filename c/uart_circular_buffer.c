/**
 * @file uart_circular_buffer.c
 * @author Bazyli Gielniak
 * @brief One of the technical interview task to do.
 * @version 0.1
 * @date 2024-02-15
 *
 * Compilation:
gcc -Wall -Wextra -Wpedantic  uart_circular_buffer.c  && ./a.out
 */
#include <stdio.h>
#include <stdbool.h>
#include <stdint.h>


/******************************************************************
 * Header functions, API
******************************************************************/

/**
 * @brief Push data to the buffer
 *
 * @returns false  if the buffer is full before adding the data, true otherwise
 */
bool Uart_Push(uint8_t data);

/**
 * @brief  Check if there is anything in the buffer
 *
 * @returns true if the buffer is empty and false otherwise.
 */
bool Uart_IsEmpty(void);

/**
 * @brief Check if buffer is full
 *
 * @returns true if the buffer is full and false otherwise.
 */
bool Uart_IsFull(void);


/**
 * @brief Pop data from the buffer
 *
 * @returns false if the buffer is empty, true if operation successful
 */
bool Uart_Pop(uint8_t* data);

/**
 * @brief Test the logic of the module
 *
 * @return true  if tests passed
 * @return false if tests failed
 */
bool Uart_selftest(void);
/******************************************************************
 * Header functions, API - END
******************************************************************/


int main(void)
{
    Uart_selftest();
}


/******************************************************************
 * Module implementation - should be in .c file
******************************************************************/
/*
Implementation notes:
- could change the context, static variables into struct
- buffer can in fact store one item less
*/

#define MAX_BUFFER_SIZE ((uint8_t)64)
static uint8_t data_buffer[MAX_BUFFER_SIZE];
static uint8_t buffer_head = 0;
static uint8_t buffer_tail = 0;
/*
head points first FREE space, adding data increments head....
tail points last entry

So:
head == tail -> EMPTY
head 0, tail 1 ->> full
head 0, tail 2 ->> almost full
head 1, tail 2 ->> full again
*/


bool Uart_selftest(void)
{
    int error_count = 0;

    // TEST full function
    bool full;
    bool expected_full = 0;

    buffer_head = 0; buffer_tail = 2; expected_full = false;
    full = Uart_IsFull(); if(expected_full != full){error_count++;}
    buffer_head = 0; buffer_tail = 1; expected_full = true;
    full = Uart_IsFull(); if(expected_full != full){error_count++;}
    buffer_head = 64; buffer_tail = 0; expected_full = true;
    full = Uart_IsFull(); if(expected_full != full){error_count++;}
    buffer_head = 63; buffer_tail = 64; expected_full = true;
    full = Uart_IsFull(); if(expected_full != full){error_count++;}
    buffer_head = 62; buffer_tail = 64; expected_full = false;
    full = Uart_IsFull(); if(expected_full != full){error_count++;}
    buffer_head = 30; buffer_tail = 0; expected_full = false;
    full = Uart_IsFull(); if(expected_full != full){error_count++;}
    buffer_head = 31; buffer_tail = 0; expected_full = false;
    full = Uart_IsFull(); if(expected_full != full){error_count++;}
    buffer_head = 32; buffer_tail = 0; expected_full = false;
    full = Uart_IsFull(); if(expected_full != full){error_count++;}
    buffer_head = 33; buffer_tail = 0; expected_full = false;
    full = Uart_IsFull(); if(expected_full != full){error_count++;}
    buffer_head = 34; buffer_tail = 0; expected_full = false;
    full = Uart_IsFull(); if(expected_full != full){error_count++;}


    // TEST push function
    buffer_head = 2; buffer_tail = 2;
    int counter = 0;
    while(true)
    {
        if(Uart_Push(0) == true)
        {
            counter ++;
        }
        else{
            break;
        }
        printf("counter = %d, head = %d, tail = %d\n", counter, (int)buffer_head, (int)buffer_tail);
    }
    printf("counter = %d\n", counter);


    if(error_count == 0)
    {
        printf("PASS\n");
        return true;
    }

    printf("FAIL!\n");
    return false;
}


bool Uart_IsFull() {
    uint8_t data_count = 0;
    if(buffer_head >= buffer_tail)
    {
        data_count = buffer_head - buffer_tail;
    }
    else
    {
        data_count = MAX_BUFFER_SIZE + buffer_head - buffer_tail;
    }

    if(data_count >= MAX_BUFFER_SIZE-1)
    {
        return true;
    }
    return false;
}

bool Uart_IsEmpty() {
    if(buffer_head == buffer_tail)
    {
        return true;
    }
    return false;
}

bool Uart_Push(uint8_t data) {
    if(Uart_IsFull() == true)
    {
        return false;
    }
    data_buffer[buffer_head] = data;
    buffer_head = (buffer_head+1) % MAX_BUFFER_SIZE;
    return true;
}

bool Uart_Pop(uint8_t* data) {
    if(data == NULL)
    {
        return false;
    }
    if(Uart_IsEmpty() == true)
    {
        return false;
    }
    *data = data_buffer[buffer_tail];
    buffer_tail = (buffer_tail+1) % MAX_BUFFER_SIZE;
    return true;
}