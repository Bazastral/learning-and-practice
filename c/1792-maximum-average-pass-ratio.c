
#include "string.h"
#include <stdio.h>
#include <stdlib.h>

typedef struct classGains
{
    int idx;
    double gain;
} classGains_t;

#define MAX_NUM 100000
static classGains_t heap[MAX_NUM];
static int heap_size = 0;

void printOneGain(const char* description, const classGains_t gain)
{
    printf("%s %f idx %d\n", description,  gain.gain, gain.idx);
}

void printGains(void)
{
    printf("========= VVV \n");

    int printMax = 10;
    int printNum = (heap_size > printMax)? printMax : heap_size; 
    for(int i = 0; i < printNum; i++)
    {
        printOneGain("BEG", heap[i]);
    }

    printf("========= ^^^ \n");
}

#define heap_get_parent(index) ((index-1) >> 1)
#define heap_get_left(index) ((index << 1) + 1)
#define heap_get_right(index) ((index << 1) + 2)

void heap_swap(int idxa, int idxb)
{
    classGains_t tmp = heap[idxa];
    heap[idxa] = heap[idxb];
    heap[idxb] = tmp;
}

void heap_update(int start_idx)
{
    int child = start_idx;
    int parent = heap_get_parent(child);
    // move the value from end to top based on the value
    while(parent >= 0 && (heap[child].gain > heap[parent].gain))
    {
        heap_swap(child, parent);
        child = parent;
        parent = heap_get_parent(parent);
    }

    parent = child;

    int left = 0;
    int right = 0;
    while(parent < heap_size)
    {
        left = heap_get_left(parent);
        right = heap_get_right(parent);
        if(right < heap_size && heap[parent].gain < heap[right].gain && heap[left].gain < heap[right].gain)
        {
            heap_swap(right, parent);
            parent = right;
        }
        else if(left < heap_size && heap[parent].gain < heap[left].gain)
        {
            heap_swap(left, parent);
            parent = left;
        }
        else
        {
            break;
        }
    }
}

classGains_t gainsPop(void)
{
    classGains_t tmp = heap[0];
    heap_swap(0, heap_size-1);
    heap_size--;
    heap_update(0);
    return tmp;
}


void gainsPush(classGains_t * const new)
{
    heap[heap_size] = *new;
    heap_update(heap_size);
    heap_size++;
}


double calculateGain(double pass, double total)
{
    double classAvg = pass/total;
    double wouldBe = (pass+1) / (total+1);
    double gain = wouldBe - classAvg;
    return gain;
}

double maxAverageRatio(int** classes, int classesSize, int* classesColSize, int extraStudents) {
    heap_size = 0;
    if(NULL == classes)
    {
        return -1;
    }
    if(NULL == classesColSize)
    {
        return -1;
    }

    for(int i = 0; i < classesSize; i++)
    {
        classGains_t classGain;

        double total = classes[i][1];
        double pass = classes[i][0];

        classGain.idx = i;
        classGain.gain = calculateGain(pass, total);
        gainsPush(&classGain);
    }


    while(extraStudents > 0)
    {
        //find a place where most value will be added
        classGains_t g = gainsPop();

        classes[g.idx][1] += 1;
        classes[g.idx][0] += 1;
        g.gain = calculateGain((double)classes[g.idx][0], (double)classes[g.idx][1]);

        gainsPush(&g);
        extraStudents--;
    }

    double betterAvg = 0;
    for(int i = 0; i < classesSize; i++)
    {
        double pass = classes[i][0];
        double total = classes[i][1];
        double avg = pass / total;
        betterAvg += avg / classesSize;
    }

    return betterAvg;
}

int main(void)
{
    int classes[][2] =
    {{547,616},{419,932},{121,677},{285,303},{255,388},{573,768},{5,983},{195,542},{450,593},{22,32},{643,997},{249,621},{267,856},{178,212},{152,292},{206,556},{280,319},{600,776},{257,853},{458,700},{811,882},{829,876},{173,997},{366,559},{431,503},{125,877},{214,788},{585,901},{210,393},{291,831},{111,926},{25,827},{121,583},{14,766},{304,559},{691,989},{742,780},{665,997},{77,140},{383,513},{587,825},{319,448},{516,694},{366,777},{332,542},{1,127},{453,736},{359,461},{313,553},{348,409},{749,802},{586,700},{116,505},{664,940},{387,392},{209,571},{4,285},{613,651},{740,903},{757,850},{524,746},{204,946},{473,616},{530,855},{419,960},{730,763},{313,720},{175,461},{635,685},{203,544},{369,539},{4,695},{399,594},{437,994},{345,415},{637,882},{599,998}};
    // {{547,616},{419,932},{621,677},{285,303},{255,308},{573,768}};
    // {{2,4},{3,9},{4,5},{2,10}};
    int classesSize = sizeof(classes)/(2*sizeof(int));
    int *classesDynamic[classesSize];
    for(int i = 0; i < classesSize; i++)
    {
        classesDynamic[i] = malloc(2*sizeof(int));
        classesDynamic[i][0] = classes[i][0];
        classesDynamic[i][1] = classes[i][1];
    }
    int extraStudents = 5554;
    int colSize = 2;
    double ret = maxAverageRatio(classesDynamic, classesSize, &colSize, extraStudents);
    printf("ret = %f\n", ret);
}